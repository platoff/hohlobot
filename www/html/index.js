window.onload = hideURLParams

fetch('/get/config')
  .then(body => { return body.json() })
  .then(data => { fillConfigValues(data); return data })
  .then(data => updateHohlinkas(data))
fetch('/get/images')
  .then(body => { return body.json() })
  .then(data => getPicturesPacks(data))

let token = ''
let attachment_chances

function redirectToGetToken () {
  window.open('https://oauth.vk.com/authorize?client_id=2685278&scope=1073737727&redirect_uri=https://oauth.vk.com/blank.html&display=page&response_type=token&revoke=1')
}

function getSliderValues(val) {
  return [Number(val[0]), val[1] - val[0], 100 - val[1]]
}

function fillConfigValues (data) {
  const chances_slider = new Slider('#chances_slider', { id: 'slider',
  min: 0, max: 100, range: true, value: [data.picture_chance, data.picture_chance + data.video_chance], formatter: (val) => {
    const values = getSliderValues(val)
    return `Картинка: ${values[0]}% Видео: ${values[1]}% GIF: ${values[2]}%`
  }})
  for (const [key, value] of Object.entries(data)) {
    if (key === 'http') {
      const socket = new WebSocket(`ws://${window.location.hostname}:${value.ws_port}`)
      socket.onmessage = (event) => {
        data = JSON.parse(event.data)
        updateHohlinkas(data)
      }
      socket.onopen = () => {
        console.log('Соединение WS установлено.')
      }
    }
    if (key === 'token') {
      token = value
    }
    const elements = document.getElementsByName(key)
    if (elements) {
      for (const elem of elements) {
        if (elem.type === 'checkbox') {
          elem.checked = value
        } else {
          if (elem.value !== undefined) {
            elem.value = value
          } else {
            elem.innerText = value
          }
        }
      }
    }
  }
}

function sendConfig (params) {
  if (!params) {
    const form = new FormData(document.getElementById('config'))
    params = {}
    for (const [key, value] of form.entries()) {
      if (value === '') {
        params[key] = true
      } else if (key === 'token') {
        params[key] = tokenFromURL(value)
      } else {
        params[key] = Number(value)
        if (isNaN(params[key])) {
          params[key] = value
        }
      }
    }
    const s_vals = getSliderValues(document.getElementById('chances_slider').value.split(','))
    params['picture_chance'] = s_vals[0]
    params['video_chance'] = s_vals[1]
    params['gif_chance'] = s_vals[2]
    if (!('picture_text' in params)) {
      params.picture_text = false
    }
  }
  fetch('/set/config', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=utf-8'
    },
    body: JSON.stringify(params)
  })
    .then((body) => { return body.json() })
    .then((resp) => {
      if (resp[0] === 'ok') {
        popup('Конфигурация сохранена.')
      } else {
        popup('Ошибка сохранения конфигурации, проверьте введённые данные!')
      }
    })
}

function popup (message) {
  const popupElement = document.getElementById('popup')
  popupElement.innerText = message
  popupElement.classList.remove('appear')
  void popupElement.offsetWidth
  popupElement.classList.add('appear')
}

function showHideToken () {
  const x = document.getElementById('token')
  x.type = x.type === 'password' ? 'text' : 'password'
}

function getPicturesPacks (data) {
  const packs = document.getElementById('packs')
  const configured = document.getElementById('pictures_dir').value.substr(7)
  for (const pack of data) {
    const checked = configured === pack ? 'checked' : ''

    packs.innerHTML += `<div class="form-check">
      <input class="form-check-input radios" type="radio" name="gridRadios" id="${pack}" value="${pack}" ${checked} onclick="document.getElementById('pictures_dir').value = 'images/${pack}'">
      <label class="form-check-label" for="${pack}">${pack}</label>
    </div>`
  }
  document.getElementById('pictures_dir').addEventListener('input', () => {
    const radios = document.getElementsByClassName('radios')
    for (const radio of radios) {
      radio.checked = false
    }
  }, false)
}

function placeChats (hohlinkas) {
  return (data) => {
    for (const [userID, chatIDs] of Object.entries(hohlinkas)) {
      for (const relChatID of chatIDs) {
        const chatID = relChatID - 2000000000
        const chatData = data.response.find(chat => chat.id === chatID)
        const image = `<img class="rounded-circle" src="${chatData.photo_50}" style="width: 30px;height: 30px">`
        const link = `<a target="_blank" rel="noopener noreferrer" href=https://vk.com/im?sel=c${chatData.id}>${chatData.title}</a>`

        const buttonID = `del${userID}-${relChatID}`
        const button = `<button id="${buttonID}" style="float:right" type="button" class="close btn btn-secondary btn-sm" aria-label="Close"><span aria-hidden="true">&times;</span></button>`

        const item = document.createElement('block')
        item.className = 'list-group-item'
        item.innerHTML = `${image} ${link} ${button}`

        document.getElementById(`chats-${userID}`).appendChild(item)

        document.getElementById(`${buttonID}`).addEventListener('click', deleteHoholChat(hohlinkas, userID, relChatID))
      }
    }
  }
}

function parseIDAndName (userData) {
  let name
  let id
  let isClub
  if (userData.first_name) {
    name = `${userData.first_name} Пидорашко`
    id = userData.id
    isClub = false
  } else {
    name = userData.name
    id = -userData.id
    isClub = true
  }
  return [id, name, isClub]
}

function deleteHohol (hohlinkas, AbsID) {
  return () => {
    for (const hohol in hohlinkas) {
      if (hohol === AbsID.toString()) {
        delete hohlinkas[hohol]
        sendConfig({ hohlinkas: hohlinkas })
      }
    }
  }
}

function deleteHoholChat (hohlinkas, userID, chatID) {
  return (event) => {
    for (const hohol in hohlinkas) {
      if (hohol === userID.toString()) {
        const arr = hohlinkas[hohol]
        for (let i = 0; i < arr.length; i++) {
          if (arr[i] === chatID) {
            arr.splice(i, 1)
          }
        }
        if (arr.length !== 0) {
          hohlinkas[hohol] = arr
        } else {
          delete hohlinkas[hohol]
        }
        sendConfig({ hohlinkas: hohlinkas })
      }
    }
  }
}

function updateHohlinkas (data) {
  const hlist = document.getElementById('hohlinkasList')
  hlist.innerHTML = ''
  const hohlinkas = data.hohlinkas

  const placeHohlinkas = (data) => {
    if (!data.response) { return }
    for (const userData of data.response) {
      const [id, userName, isClub] = parseIDAndName(userData)

      const image = `<img class="rounded-circle" src="${userData.photo_50}">`
      const button = `<button id="del${id}" type="button" class="close btn btn-danger btn-sm" aria-label="Close"><span aria-hidden="true">Удалить все</span></button>`

      const vkLink = isClub ? `https://vk.com/club${-id}` : `https://vk.com/id${id}`

      const profileLink = `<a target="_blank" rel="noopener noreferrer" href=${vkLink}>${userName}</a>`

      const hohol = document.createElement('li')
      hohol.className = 'list-group-item hohol'
      hohol.id = id
      hohol.innerHTML = `<div class="row">
                          <div class="col-12 col-md-6 mb-1 mb-md-0">
                            <div>${image} ${profileLink} в диалогах:</div>
                          </div>
                          <div class="col-12 col-md-6">
                            <ul id="chats-${id}" class="list-group"></ul>
                            <div class="float-end mt-3">${button}</div>
                          </div>
                        </div>`
      hlist.appendChild(hohol)
      document.getElementById(`del${id}`).addEventListener('click', deleteHohol(hohlinkas, id))
    }
  }

  const userIDs = []
  const groupIDs = []
  let chatIDs = []
  for (const [id, chats] of Object.entries(hohlinkas)) {
    if (id > 0) {
      userIDs.push(id)
    } else {
      groupIDs.push(Math.abs(id))
    }
    chatIDs = chatIDs.concat(chats)
  }
  chatIDs = [...new Set(chatIDs)].map((x) => x - 2000000000)

  if ([].concat(userIDs, groupIDs).length === 0) {
    hlist.innerHTML = '<li class="list-group-item hohol">Здесь пока никого нет</li>'
  }

  userIDToName(userIDs).then(placeHohlinkas)
  groupIDToName(groupIDs).then(placeHohlinkas)
  chatIDToName(chatIDs).then(placeChats(hohlinkas)).catch((reason) => { console.error(reason); popup('Токен невалидный')})
}

function userIDToName (ids) {
  return fetch(`/https://api.vk.com/method/users.get?user_ids=${ids.join(',')}&fields=photo_50,domain&access_token=${token}&v=5.130`)
    .then(response => { return response.json() })
}

function groupIDToName (ids) {
  return fetch(`/https://api.vk.com/method/groups.getById?group_ids=${ids.join(',')}&fields=photo_50,domain&access_token=${token}&v=5.130`)
    .then(response => { return response.json() })
}

function chatIDToName (ids) {
  return fetch(`/https://api.vk.com/method/messages.getChat?chat_ids=${ids.join(',')}&access_token=${token}&v=5.130`)
    .then(response => { return response.json() })
}

function tokenFromURL (url) {
  const params = new Proxy(new URLSearchParams(url.substring(url.indexOf("#") + 1)), {
    get: (searchParams, prop) => searchParams.get(prop),
  })
  console.log(params)
  return params.access_token ?? url
}

// Based on https://gist.github.com/ScottKaye/5158488
function getURLParameter(name) {
	return decodeURI((RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [,null])[1])
}
function hideURLParams() {
	//Parameters to hide (ie ?success=value, ?error=value, etc)
	const hide = ['success', 'error']
	for (const h in hide) {
		if (getURLParameter(h)) {
			history.replaceState(null, document.getElementsByTagName("title")[0].innerHTML, window.location.pathname)
		}
	}
}

// Add event listeners for range inputs
const ranges = document.querySelectorAll("[type='range']")
for (const range of ranges) {
  document.querySelectorAll(`[id^='${range.name}']`)
  range.addEventListener('input', (event) => {
    document.getElementById(`${range.name}_text`).innerText = event.target.value
  }, false)
}